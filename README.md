# usenet3
A distributed, anti-fragile, Usenet-style system of discussion groups

See <a href='https://gitlab.com/aaron-baugher/usenet3/wikis/pages'>the
wiki</a> for design and planning documents (that's all there is
so far).

This is little more than the germ of an idea now, sparked in part by a <a href="http://voxday.blogspot.com/2015/07/the-joke-aint-over.html?showComment=1438158119022#c674289039328636358">conversation at Vox Populi</a>.  Ever since the web supplanted Usenet, and as I've dealt with annoying web forum software over the years, I've wished for some way to "bring back Usenet."  Combine that with some thoughts about how single sites like Reddit and Twitter are susceptible to SJW infiltration and takeover, or government intrusion and control.

So how could you do a:

- web-based (so people who know nothing but the browser can use it)
- distributed (so there isn't a single point of failure)
- modern (utf-8, secure, privacy-respecting, mobile)

version of Usenet?

I'll put further details into an ideas document or three, <a
href='https://gitlab.com/aaron-baugher/usenet3/wikis/pages'>on the wiki</a> since I discovered it can be cloned and edited locally.  

Calling it "usenet3" for now, since there was already a Usenet 2 (or Usenet II) that never went anywhere.
