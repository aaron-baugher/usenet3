A mind-dump of ideas I've had about this, plus some new ones that are coming to mind now that I get it down in writing.  I'm going to try FAQ format.

Q. What is the purpose of Usenet 3?

A. To "bring back Usenet."  Usenet is superior to web-based forums in several ways, but it's been largely abandoned.  I want to determine whether the advantages of Usenet could be brought back in a way that will appeal to the modern user, while also building in defenses against modern dangers.

Q. What are those advantages of Usenet over web forums?

A. The main ones that come to mind:

-   Distributed.  
    -   No one owns it, so it can't be controlled by blackmail or one SJW owner/manager.
    -   It doesn't require major funding, since it grows only as fast as "leaf" sites fund themselves.
    -   No single point of failure/control.
-   Open.
    -   For the most part, it's wide open.  
        -   There are moderated groups, but the moderation is done internally, and there are always unmoderated alternatives.
    -   It was possible to create new groups, opening up new topics.
-   Simplicity.
    -   The text-only nature and the fact that it did one thing very well meant that newsreaders were available for all systems, and of various styles.
    -   Web forums often get cluttered up with a lot of other stuff, and you spend more time scrolling and paging.
-   Threading and quoting.
    -   Threading and easy and consistent quoting conventions made it easy to follow the conversation.
-   Client/server.
    -   The clean separation between client and server, with NNTP as the API between them, means you can create a client and it will work with any server, and vice versa.  That makes both ends simpler to do, and multiplies effort.  It also creates consistency; even though different clients might have different features, what features they had were consistent from one to another.
    -   On web forums, there's no separation, everything comes from the server.  Even any code that runs in the browser is closely tied to the server.  So you can't create a new client that will talk to multiple existing forums/blogs.
-   Kill-files.
    -   Some web forums let you ignore people, but kill-filing was more ubiquitous and easier to use.  A troll could quickly be ignored by everyone, making it harder for them to get attention.
-   (More?)

Q. If Usenet is so great, why did people stop using it?

A. Well, it's not because people started living in the web browser.  Browsers had built-in newsreaders for a long time, and there were nntp:// links on the web, so people could have used it.  Browser newsreaders weren't great, but they were more functional than any web forum, especially back then.  So some other possible reasons:

-   Fragmentation.
    -   Web forums were typically targeted to a topic, while a user popping up a newsreader for the first time was overwhelmed with tens of thousands of groups.  It took research to figure out what groups to subscribe to.
-   Ugliness.
    -   Usenet was text-only.  You could transmit images through it, but they didn't automatically pop up in the reader.  No avatars, no LOLcats.  Nowadays, with the popularity of texting, a text-only system might go over better; but in the days when everyone was loading up web sites with blinking text and animated GIFs, text automatically looked old and ugly.
-   Size.
    -   Maintaining a full-feed newsserver was a significant investment.  I worked for an ISP in the late 90s, and we spent a lot of time on that one thing, both adding drive space and looking for ways to keep up with the bandwidth requirements without breaking the budget.  ISPs weren't sad to see it go.
-   Scary stuff.
    -   There was/is a lot of offensive stuff on Usenet, and no way to filter it.  So it wasn't something ISPs were anxious to promote.  You don't want to be showing a new customer all the features of this new "Internet" thing and hear him say, "Hmm, 'alt.sex.stories.cuckold'&#x2026; what's that mean?"  So that's the other reason ISPs were glad to get out of the Usenet business.
-   (More?)

Q. What is the more detailed goal of Usenet 3?

A. The goal here is ?-fold:

-   Anti-fragile.
    -   Taking down one server, or a number of servers, should not stop service for anyone else, unless he only has one neighbor on the system.
        -   In fact, with wireless devices as potential servers, this has to be assumed as a regular condition to be handled, not an emergency case.
    -   So at the application level, it has to know how to connect to multiple servers and get what it needs while ignoring what it has.
    -   And by default, it should connect to (and serve data to) as many neighbors as possible.
    -   It should do all this as invisibly as possible (while allowing the user to control and monitor it if he wishes).
-   Open.
    -   See above about Usenet.  Moderation should be done voluntarily and internally at the group level, or as self-moderation in the client.
    -   New clients and servers should be welcome as long as they meet the spec.
        -   (\*\* Maybe there should be an official RFC at some point.)
-   Ease of communication.
    -   The main purpose is to allow people to communicate, so it should aid that and stay out of the way of it as much as possible.
    -   It should be easy to 
        -   post,
        -   quote,
        -   follow the threads of a conversation,
        -   and pick back up where you left off last time.
-   Immunity to entryism.
    -   I think the distributed nature and some of the other features help with this, but it should be a specific question to keep in mind at all times:
        -   "Are there any weak points in the plan that could give SJWs a foot in the door?"
-   Security & privacy.
    -   Server-server and client-server communications should be encrypted as much as possible.  
        -   Not that anyone should be posting sensitive data to a public forum; but it's just a good idea these days to encrypt as much as possible, to make it harder for snoops to know what to dig at.
    -   Public key signatures should be encouraged and made easy in clients.
    -   Ditto encryption of individual messages.
        -   One way to make a group private: encrypt all messages, and give the key to the members only.
    -   Those things were done sometimes by external programs before, but some thought might be given to supporting them in the application or transport level.
-   Profit!!!
    -   I wouldn't mind making a little money off it.  That's not the goal, but it might grow better if there were some way to monetize it.  Maybe someone with plenty of bandwidth and a good router could sell connections to other people in his building, for instance.  He'd be more likely to do that if there were a built-in way for him to charge them.
    -   But that's really a secondary (or tertiary) consideration for much later.  Just throwing it in.

Q. So what features of Usenet 3 could attract people and keep them?

A. I'll focus on the user features here and save technical under-the-hood features for another question.

-   Simplicity.
    -   It still needs to be simple.  Services like Twitter, or the chat in Facebook, show that people want things kept simple.
    -   So it should be simple to install and start using &#x2013; basically one click, if possible.
    -   The number of groups needs to be kept reasonable somehow so the newbie isn't overwhelmed.  Maybe something along the Reddit model, where you have the main official groups, then people can create their own side-groups willy-nilly, but the new user searching just sees the main groups at first.  It should be extensible, but not chaos.
    -   Posting/viewing binaries should be easy; no uudecoding.
        -   (Though whether/how to support binaries is a topic unto itself.)
-   Pretty and fun.
    -   It needs to look nice.  That means people need to be able to have icons/avatars and use emojis and all that stuff.
    -   If it has a client-server model, someone could make a bare-bones client without those things, but the initial client should have them, and the underlying protocol should support them.
-   Social features.
    -   People expect to be able to PM/DM each other.  So that might need to be part of the protocol, or maybe use a secondary protocol that runs in parallel.
    -   In Emacs/Gnus, kill-filing was replaced with scoring, which allows you to up/down-vote individual authors, subjects, or threads, which results in a group being sorted to provide you with your favorite authors/topics first.  That was unusual then, but would come naturally to users today.
-   Accessibility.
    -   Unicode for internationalization.
    -   Works on mobile devices.
    -   Web-based.  Other clients/devices are great, but there should be a web client for people to find and use by default.
-   (More?)

Q. What technical features should it have?

A. Keeping in mind the goals a couple questions back:

-   Client-server.
    -   This seems like the way to go to me, but I'm open to other options.
-   A standard transport protocol.
    -   The [NNTP protocol](https://tools.ietf.org/html/rfc977) is probably a good place to start, but may need an update.
    -   It used to be 7-bit.  I know there were efforts to make it 8-bit, but I don't know if they went anywhere.
    -   In any case, it would need to support Unicode.
    -   Due to firewalls, it may be good to do everything through port 80, which might necessitate further changes.
    -   And encrypt it.
    -   UUCP could be another option, where TCP is not available (as appears to be the case with Bluetooth).
    -   And here's the RFC on [transferring Usenet messages](https://tools.ietf.org/html/rfc850) regardless of transport method.
    -   Like I mentioned above, we'd probably want to write an official RFC to define the standard at some point.
-   Multi-path server-server routing.
    -   A server should be able to use as many nearby servers as possible, gracefully getting what it needs from each one and sending them all what they need.
        -   A load-sharing algorithm might be good.
            -   And maybe servers should be able to tell each other what they can handle, so they can choose the one with the most resources.
-   Only get what you need.
    -   Most servers should probably act as leaf servers, only getting what has been requested by clients or neighboring servers.  That will keep the bandwidth and space needs lower than everyone getting everything all the time.
    -   So Bob connects to Adam and subscribes to 10 groups, so his system pulls them automatically whenever he's online.  Then Carl connects to Bob and requests 3 groups that Bob wasn't getting, so Bob's server starts pulling those 3 too and passing them along to Carl.
    -   When no one has asked a server for a group in a while (say maybe 2 weeks by default) it drops it.
    -   And in the meantime, if Carl connects to Dave, he'll also pull those groups through him.
    -   See [Leafnode](http://leafnode.org/) for an example of this 'leaf server' concept.
-   Moving on to more client technical considerations:
-   Threading.
    -   Done right, threading is the best way to follow a conversation.  It's just rarely done right on the web.
    -   But maybe turning it off should be an option.
-   Kill-filing/scoring.  
    -   Call up-scoring a user/thread "following" for familiarity.
-   Support images, icons/avatars, emojis.
    -   Fail gracefully on anything not supported.
-   (More?)

(much more to come)


- Not sure where this fits in, but I was exploring gopher today (yes, gopher
  servers still exist!), and I was shocked by the speed.  It was so fast I
  literally thought maybe it was reading from local files on my system at
  first.  We get used to the delay of web pages with multiple connections,
  sometimes dozens or hundreds of images and other resources per page; so
  even with 20ms latency like I typically have, there's often a wait.  It's
  amazing how fast a single transfer can be.


