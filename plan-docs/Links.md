I'm collecting useful links here, so feel free to add:

-   [An article on the "port-80fication" of services like Usenet.](https://medium.com/@maradydd/on-port-80-d8d6d3443d9a)
-   [The post at Vox Populi that spurred me to explore this idea.](http://voxday.blogspot.com/2015/07/the-joke-aint-over.html?showComment=1438164836687#c674289039328636358)  Several good ideas there.
-   [A distributed cloud system.](http://storj.io/)
-   [Diaspora](https://diasporafoundation.org/) - a distributed social media network (alternative to Facebook).
-   [Leafnode](http://leafnode.org/), a 'leaf' Usenet server with some ideas that might be useful.
-   [RFC 850](https://tools.ietf.org/html/rfc850) - Original Standard for interchange of USENET messages.
-   [RFC 1036](https://tools.ietf.org/html/rfc1036) - Update to RFC 850.
-   [RFC 977](https://tools.ietf.org/html/rfc977) - NNTP, Network News Transfer Protocol.
-   [RFC 3977](https://tools.ietf.org/html/rfc3977) - An update to NNTP.
-   [RFC 2980](https://tools.ietf.org/html/rfc2980) - Common NNTP Extensions.
-   [UUCP](https://en.wikipedia.org/wiki/UUCP)

-   [Web is Dead](gopher://gopher.floodgap.com:70/0/gopher/webisdead.txt) -
	An article on Wired from a few years ago, about how traffic is moving
	away from the web browser to apps and other services.  It's a gopher
	link; not sure what browsers will handle it.  I should find a web link
	for it.  Anyway, it jibed with this idea in some ways.

